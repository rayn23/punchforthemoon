﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepotPlayerUn : MonoBehaviour {

	void OnTriggerEnter (Collider col) 
	{
		if ((GameObject.Find ("JoueurUn").GetComponent<PersoUn> ().ItemTower == true) && (col.gameObject.tag == "PlayerUn")) 
		{
			GameObject.Find ("JoueurUn").GetComponent<PersoUn> ().ItemTower = false;
			GameObject.Find ("JoueurUn").GetComponent<DeplacementPersoUn> ().vitesse = 150;
		}
	}
}
