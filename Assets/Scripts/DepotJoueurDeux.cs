﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepotJoueurDeux : MonoBehaviour {
	void OnTriggerEnter (Collider col) 
	{
		if ((GameObject.Find ("JoueurDeux").GetComponent<PersoDeux> ().ItemTower == true) && (col.gameObject.tag == "PlayerDeux")) 
		{
			GameObject.Find ("JoueurDeux").GetComponent<PersoDeux> ().ItemTower = false;
			GameObject.Find ("JoueurDeux").GetComponent<DeplacementPersoUn> ().vitesse = 150;
		}
	}
}
