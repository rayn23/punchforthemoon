﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleForTower : MonoBehaviour {
	


		void Update () 
		{
			transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime);
		}

	    void OnTriggerEnter (Collider col) // collision pour que le joueur un recup l'object
	    {
		if ((GameObject.Find ("JoueurUn").GetComponent<PersoUn> ().ItemTower == false)&& (col.gameObject.tag == "PlayerUn"))
		{
			GameObject.Find ("JoueurUn").GetComponent<PersoUn> ().ItemTower = true;
			GameObject.Find ("JoueurUn").GetComponent<DeplacementPersoUn> ().vitesse = 90;
			Destroy (gameObject);
		}
		if ((GameObject.Find ("JoueurDeux").GetComponent<PersoDeux> ().ItemTower == false)&& (col.gameObject.tag == "PlayerDeux"))
		{
			GameObject.Find ("JoueurDeux").GetComponent<PersoDeux> ().ItemTower = true;
			GameObject.Find ("JoueurDeux").GetComponent<DeplacementPersoUn> ().vitesse = 90;
			Destroy (gameObject);
		}
	}
}
