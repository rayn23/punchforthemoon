﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeplacementPersoUn : MonoBehaviour 
{
	[Range(25f,500f)]public float vitesse;
	public float vitesseRotation;
	Rigidbody rb;
	Transform shape;

	public KeyCode keyUp;
	public KeyCode keyDown;
	public KeyCode keyLeft;
	public KeyCode keyRight;


	Vector3 mouvement;

	void Awake()
	{
		rb = GetComponent<Rigidbody> ();
		shape = transform.Find ("SHAPE");
	}


	void Update()
	{
		RotateTowardsVelocity ();
	}




	void FixedUpdate ()
	{
		Movement ();	
	}

	void Movement ()
	{
		mouvement = Vector3.zero;

		if (Input.GetKey (keyRight))
		{
			mouvement += Vector3.right * vitesse * Time.deltaTime;
		}

		if (Input.GetKey (keyLeft))
		{
			mouvement += Vector3.left * vitesse * Time.deltaTime;
		}


		if (Input.GetKey (keyUp))
		{
			mouvement += Vector3.forward * vitesse * Time.deltaTime;
		}

		if (Input.GetKey (keyDown))
		{
			mouvement += Vector3.back * vitesse * Time.deltaTime;
		}


		if (mouvement.magnitude > vitesse * Time.deltaTime)
			mouvement = mouvement.normalized * vitesse * Time.deltaTime;


		rb.MovePosition(transform.position + mouvement);

	} 

	void RotateTowardsVelocity()
	{
		if (mouvement != Vector3.zero)
		{
			Quaternion newRotation = Quaternion.Slerp(shape.rotation,Quaternion.LookRotation(mouvement.normalized), vitesseRotation * Time.deltaTime);
			shape.rotation = newRotation;
			//Debug.Log (mouvement);
		}
	}

}